class CreateHargas < ActiveRecord::Migration
  def change
    create_table :hargas, id: :uuid do |t|
      t.string :kota_tujuan
      t.decimal :tarif, precision: 18, scale: 2
      t.decimal :ongkos, precision: 18, scale: 2

      t.timestamps null: false
    end
  end
end

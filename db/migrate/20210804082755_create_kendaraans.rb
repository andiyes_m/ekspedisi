class CreateKendaraans < ActiveRecord::Migration
  def change
    create_table :kendaraans, id: :uuid do |t|
      t.string :no_kendaraan
      t.string :tipe
      t.string :supir

      t.timestamps null: false
    end
  end
end

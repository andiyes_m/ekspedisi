class CreateKonsumen < ActiveRecord::Migration
  def change
    create_table :konsumen, id: :uuid do |t|
      t.string :name
      t.string :status

      t.timestamps null: false
    end
  end
end

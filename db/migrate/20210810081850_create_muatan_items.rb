class CreateMuatanItems < ActiveRecord::Migration
  def change
    create_table :muatan_items, id: :uuid do |t|
      t.string :no_do
      t.uuid :konsumen_id
      t.decimal :tambahan, precision: 18, scale: 2
      t.uuid :daftar_muatan_id

      t.timestamps null: false
    end
    add_foreign_key :muatan_items, :konsumen, column: :konsumen_id
    add_index :muatan_items, :konsumen_id
  end
end

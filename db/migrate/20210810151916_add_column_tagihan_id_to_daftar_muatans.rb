class AddColumnTagihanIdToDaftarMuatans < ActiveRecord::Migration
  def change
    add_column :daftar_muatans, :tagihan_id, :uuid, null: true
    add_foreign_key :daftar_muatans, :tagihans
    add_index :daftar_muatans, :tagihan_id
  end
end

class CreateTagihans < ActiveRecord::Migration
  def change
    create_table :tagihans, id: :uuid do |t|
      t.string :customer_name
      t.string :no_tagihan
      t.date :tgl_tagihan

      t.timestamps null: false
    end
  end
end

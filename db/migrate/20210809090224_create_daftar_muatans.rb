class CreateDaftarMuatans < ActiveRecord::Migration
  def change
    create_table :daftar_muatans, id: :uuid do |t|
      t.string :customer_name
      t.uuid :kendaraan_id
      t.string :no_dm
      t.date :tgl_kirim
      t.string :kota
      t.decimal :harga, precision: 18, scale: 2
      t.decimal :tambahan, precision: 18, scale: 2
      t.date :tgl_terima

      t.timestamps null: false
    end
    add_foreign_key :daftar_muatans, :kendaraans
    add_index :daftar_muatans, :kendaraan_id
  end
end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20210810151916) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "daftar_muatans", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "customer_name"
    t.uuid     "kendaraan_id"
    t.string   "no_dm"
    t.date     "tgl_kirim"
    t.string   "kota"
    t.decimal  "harga",         precision: 18, scale: 2
    t.decimal  "tambahan",      precision: 18, scale: 2
    t.date     "tgl_terima"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.uuid     "tagihan_id"
  end

  add_index "daftar_muatans", ["kendaraan_id"], name: "index_daftar_muatans_on_kendaraan_id", using: :btree
  add_index "daftar_muatans", ["tagihan_id"], name: "index_daftar_muatans_on_tagihan_id", using: :btree

  create_table "hargas", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "kota_tujuan"
    t.decimal  "tarif",       precision: 18, scale: 2
    t.decimal  "ongkos",      precision: 18, scale: 2
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "kendaraans", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "no_kendaraan"
    t.string   "tipe"
    t.string   "supir"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "konsumen", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "muatan_items", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "no_do"
    t.uuid     "konsumen_id"
    t.decimal  "tambahan",         precision: 18, scale: 2
    t.uuid     "daftar_muatan_id"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "muatan_items", ["konsumen_id"], name: "index_muatan_items_on_konsumen_id", using: :btree

  create_table "tagihans", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "customer_name"
    t.string   "no_tagihan"
    t.date     "tgl_tagihan"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_foreign_key "daftar_muatans", "kendaraans"
  add_foreign_key "daftar_muatans", "tagihans"
  add_foreign_key "muatan_items", "konsumen", column: "konsumen_id"
end

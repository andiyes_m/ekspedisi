json.extract! konsuman, :id, :uuid, :name, :status, :created_at, :updated_at
json.url konsuman_url(konsuman, format: :json)

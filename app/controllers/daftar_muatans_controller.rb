class DaftarMuatansController < ApplicationController
  before_action :set_daftar_muatan, only: [:show, :edit, :update, :destroy]

  # GET /daftar_muatans
  # GET /daftar_muatans.json
  def index
    @daftar_muatans = DaftarMuatan.paginate(page: params[:page], per_page: 20)
  end

  # GET /daftar_muatans/1
  # GET /daftar_muatans/1.json
  def show
  end

  # GET /daftar_muatans/new
  def new
    @daftar_muatan = DaftarMuatan.new
    @kendaraan = Kendaraan.all
    @harga = Harga.all
    @konsuman = Konsuman.all
    @arr = []

    (1..15).each do |i|
      @arr << MuatanItem.new
    end
  end

  # GET /daftar_muatans/1/edit
  def edit
    @kendaraan = Kendaraan.all
    @harga = Harga.all
    @konsuman = Konsuman.all
    @arr = []

    i = 1
    @daftar_muatan.muatan_items.each do |muatan_item|
      @arr << muatan_item
    end

    (i..15).each do |i|
      @arr << MuatanItem.new
    end
  end

  # POST /daftar_muatans
  # POST /daftar_muatans.json
  def create
    @daftar_muatan = DaftarMuatan.new(daftar_muatan_params)

    respond_to do |format|
      if @daftar_muatan.save
        @daftar_muatan.reload
        (1..15).each do |i|
          break if params[("no_do_" + i.to_s).to_sym].blank?
          MuatanItem.create(
            no_do: params[("no_do_" + i.to_s).to_sym],
            konsumen_id: params[("konsumen_id_" + i.to_s).to_sym],
            tambahan: params[("tambahan_" + i.to_s).to_sym],
            daftar_muatan_id: @daftar_muatan.id
          )
        end
        format.html { redirect_to @daftar_muatan, notice: 'Daftar muatan was successfully created.' }
        format.json { render :show, status: :created, location: @daftar_muatan }
      else
        format.html { render :new }
        format.json { render json: @daftar_muatan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /daftar_muatans/1
  # PATCH/PUT /daftar_muatans/1.json
  def update
    respond_to do |format|
      if @daftar_muatan.update(daftar_muatan_params)
        @daftar_muatan.reload
        @daftar_muatan.muatan_items.delete_all
        (1..15).each do |i|
          break if params[("no_do_" + i.to_s).to_sym].blank?
          MuatanItem.create(
            no_do: params[("no_do_" + i.to_s).to_sym],
            konsumen_id: params[("konsumen_id_" + i.to_s).to_sym],
            tambahan: params[("tambahan_" + i.to_s).to_sym],
            daftar_muatan_id: @daftar_muatan.id
          )
        end
        format.html { redirect_to @daftar_muatan, notice: 'Daftar muatan was successfully updated.' }
        format.json { render :show, status: :ok, location: @daftar_muatan }
      else
        format.html { render :edit }
        format.json { render json: @daftar_muatan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /daftar_muatans/1
  # DELETE /daftar_muatans/1.json
  def destroy
    @daftar_muatan.muatan_items.delete_all
    @daftar_muatan.destroy
    respond_to do |format|
      format.html { redirect_to daftar_muatans_url, notice: 'Daftar muatan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_daftar_muatan
      @daftar_muatan = DaftarMuatan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def daftar_muatan_params
      params.require(:daftar_muatan).permit(:customer_name, :kendaraan_id, :no_dm, :tgl_kirim, :kota, :harga, :tambahan, :tgl_terima)
    end
end

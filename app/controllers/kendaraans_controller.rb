class KendaraansController < ApplicationController
  before_action :set_kendaraan, only: [:show, :edit, :update, :destroy]

  # GET /kendaraans
  # GET /kendaraans.json
  def index
    @kendaraans = Kendaraan.all
  end

  # GET /kendaraans/1
  # GET /kendaraans/1.json
  def show
  end

  # GET /kendaraans/new
  def new
    @kendaraan = Kendaraan.new
  end

  # GET /kendaraans/1/edit
  def edit
  end

  # POST /kendaraans
  # POST /kendaraans.json
  def create
    @kendaraan = Kendaraan.new(kendaraan_params)

    respond_to do |format|
      if @kendaraan.save
        format.html { redirect_to @kendaraan, notice: 'Kendaraan was successfully created.' }
        format.json { render :show, status: :created, location: @kendaraan }
      else
        format.html { render :new }
        format.json { render json: @kendaraan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /kendaraans/1
  # PATCH/PUT /kendaraans/1.json
  def update
    respond_to do |format|
      if @kendaraan.update(kendaraan_params)
        format.html { redirect_to @kendaraan, notice: 'Kendaraan was successfully updated.' }
        format.json { render :show, status: :ok, location: @kendaraan }
      else
        format.html { render :edit }
        format.json { render json: @kendaraan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kendaraans/1
  # DELETE /kendaraans/1.json
  def destroy
    @kendaraan.destroy
    respond_to do |format|
      format.html { redirect_to kendaraans_url, notice: 'Kendaraan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_kendaraan
      @kendaraan = Kendaraan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def kendaraan_params
      params.require(:kendaraan).permit(:no_kendaraan, :tipe, :supir)
    end
end

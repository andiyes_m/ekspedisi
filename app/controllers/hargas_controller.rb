class HargasController < ApplicationController
  before_action :set_harga, only: [:show, :edit, :update, :destroy]

  # GET /hargas
  # GET /hargas.json
  def index
    @hargas = Harga.all
  end

  # GET /hargas/1
  # GET /hargas/1.json
  def show
  end

  # GET /hargas/new
  def new
    @harga = Harga.new
  end

  # GET /hargas/1/edit
  def edit
  end

  # POST /hargas
  # POST /hargas.json
  def create
    @harga = Harga.new(harga_params)

    respond_to do |format|
      if @harga.save
        format.html { redirect_to @harga, notice: 'Harga was successfully created.' }
        format.json { render :show, status: :created, location: @harga }
      else
        format.html { render :new }
        format.json { render json: @harga.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hargas/1
  # PATCH/PUT /hargas/1.json
  def update
    respond_to do |format|
      if @harga.update(harga_params)
        format.html { redirect_to @harga, notice: 'Harga was successfully updated.' }
        format.json { render :show, status: :ok, location: @harga }
      else
        format.html { render :edit }
        format.json { render json: @harga.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hargas/1
  # DELETE /hargas/1.json
  def destroy
    @harga.destroy
    respond_to do |format|
      format.html { redirect_to hargas_url, notice: 'Harga was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_harga
      @harga = Harga.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def harga_params
      params.require(:harga).permit(:kota_tujuan, :tarif, :ongkos)
    end
end

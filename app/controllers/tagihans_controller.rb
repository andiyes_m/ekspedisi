class TagihansController < ApplicationController
  before_action :set_tagihan, only: [:show, :edit, :update, :destroy]

  # GET /tagihans
  # GET /tagihans.json
  def index
    @tagihans = Tagihan.paginate(page: params[:page], per_page: 20)
  end

  # GET /tagihans/1
  # GET /tagihans/1.json
  def show
  end

  # GET /tagihans/new
  def new
    @tagihan = Tagihan.new
    from_date = params.require(:tagihan).permit(:from_date).to_hash
    thru_date = params.require(:tagihan).permit(:thru_date).to_hash
    from_date = from_date["from_date(1i)"] + "-" + from_date["from_date(2i)"] + "-" + from_date["from_date(3i)"]
    thru_date = thru_date["thru_date(1i)"] + "-" + thru_date["thru_date(2i)"] + "-" + thru_date["thru_date(3i)"]
    @daftar_muatans = DaftarMuatan.joins(:kendaraan).where("tagihan_id IS NULL AND tgl_kirim BETWEEN '#{from_date}' AND '#{thru_date}'").order("kendaraans.tipe")
    today = Date.today
    count = DaftarMuatan.where("TO_CHAR(tgl_kirim, 'YYYY-MM') = '#{today.strftime("%Y-%m")}'").count + 1
    @tagihan.no_tagihan = count.to_s.rjust(3, "0") + "/" + today.month.to_s.rjust(2, "0") + "/UT/" + today.year.to_s
    @tagihan.customer_name = params.require(:tagihan).permit(:customer_name)[:customer_name]
  end

  def new_form
    @tagihan = Tagihan.new
  end

  # GET /tagihans/1/edit
  def edit
    @daftar_muatans = @tagihan.daftar_muatans.joins(:kendaraan).order("kendaraans.tipe")
  end

  # POST /tagihans
  # POST /tagihans.json
  def create
    @tagihan = Tagihan.new(tagihan_params)
    respond_to do |format|
      if @tagihan.save
        @tagihan.reload
        idx = 1
        while params[("dm_id_" + idx.to_s).to_sym].present? do
          dm = DaftarMuatan.find(params[("dm_id_" + idx.to_s).to_sym])
          dm.update!(tagihan_id: @tagihan.id)
          idx = idx + 1
        end
        format.html { redirect_to @tagihan, notice: 'Tagihan was successfully created.' }
        format.json { render :show, status: :created, location: @tagihan }
      else
        format.html { render :new }
        format.json { render json: @tagihan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tagihans/1
  # PATCH/PUT /tagihans/1.json
  def update
    respond_to do |format|
      if @tagihan.update(tagihan_params)
        format.html { redirect_to @tagihan, notice: 'Tagihan was successfully updated.' }
        format.json { render :show, status: :ok, location: @tagihan }
      else
        format.html { render :edit }
        format.json { render json: @tagihan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tagihans/1
  # DELETE /tagihans/1.json
  def destroy
    DaftarMuatan.where(tagihan_id: @tagihan.id).update_all(tagihan_id: nil)
    @tagihan.destroy
    respond_to do |format|
      format.html { redirect_to tagihans_url, notice: 'Tagihan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tagihan
      @tagihan = Tagihan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tagihan_params
      params.require(:tagihan).permit(:customer_name, :no_tagihan, :tgl_tagihan)
    end
end

class KonsumenController < ApplicationController
  before_action :set_konsuman, only: [:show, :edit, :update, :destroy]

  # GET /konsumen
  # GET /konsumen.json
  def index
    @konsumen = Konsuman.all
  end

  # GET /konsumen/1
  # GET /konsumen/1.json
  def show
  end

  # GET /konsumen/new
  def new
    @konsuman = Konsuman.new
  end

  # GET /konsumen/1/edit
  def edit
  end

  # POST /konsumen
  # POST /konsumen.json
  def create
    @konsuman = Konsuman.new(konsuman_params)

    respond_to do |format|
      if @konsuman.save
        format.html { redirect_to @konsuman, notice: 'Konsuman was successfully created.' }
        format.json { render :show, status: :created, location: @konsuman }
      else
        format.html { render :new }
        format.json { render json: @konsuman.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /konsumen/1
  # PATCH/PUT /konsumen/1.json
  def update
    respond_to do |format|
      if @konsuman.update(konsuman_params)
        format.html { redirect_to @konsuman, notice: 'Konsuman was successfully updated.' }
        format.json { render :show, status: :ok, location: @konsuman }
      else
        format.html { render :edit }
        format.json { render json: @konsuman.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /konsumen/1
  # DELETE /konsumen/1.json
  def destroy
    @konsuman.destroy
    respond_to do |format|
      format.html { redirect_to konsumen_url, notice: 'Konsuman was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_konsuman
      @konsuman = Konsuman.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def konsuman_params
      params.require(:konsuman).permit(:name, :status)
    end
end

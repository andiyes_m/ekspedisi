require 'test_helper'

class DeliveryOrdersControllerTest < ActionController::TestCase
  setup do
    @delivery_order = delivery_orders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:delivery_orders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create delivery_order" do
    assert_difference('DeliveryOrder.count') do
      post :create, delivery_order: { alamat: @delivery_order.alamat, harga_id: @delivery_order.harga_id, id: @delivery_order.id, keterangan: @delivery_order.keterangan, konsumen_id: @delivery_order.konsumen_id, nomor_od: @delivery_order.nomor_od, penerima: @delivery_order.penerima, qty: @delivery_order.qty, status: @delivery_order.status, tgl_diterima: @delivery_order.tgl_diterima, tgl_do: @delivery_order.tgl_do }
    end

    assert_redirected_to delivery_order_path(assigns(:delivery_order))
  end

  test "should show delivery_order" do
    get :show, id: @delivery_order
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @delivery_order
    assert_response :success
  end

  test "should update delivery_order" do
    patch :update, id: @delivery_order, delivery_order: { alamat: @delivery_order.alamat, harga_id: @delivery_order.harga_id, id: @delivery_order.id, keterangan: @delivery_order.keterangan, konsumen_id: @delivery_order.konsumen_id, nomor_od: @delivery_order.nomor_od, penerima: @delivery_order.penerima, qty: @delivery_order.qty, status: @delivery_order.status, tgl_diterima: @delivery_order.tgl_diterima, tgl_do: @delivery_order.tgl_do }
    assert_redirected_to delivery_order_path(assigns(:delivery_order))
  end

  test "should destroy delivery_order" do
    assert_difference('DeliveryOrder.count', -1) do
      delete :destroy, id: @delivery_order
    end

    assert_redirected_to delivery_orders_path
  end
end

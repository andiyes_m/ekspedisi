require 'test_helper'

class KonsumenControllerTest < ActionController::TestCase
  setup do
    @konsuman = konsumen(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:konsumen)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create konsuman" do
    assert_difference('Konsuman.count') do
      post :create, konsuman: { name: @konsuman.name, status: @konsuman.status, uuid: @konsuman.uuid }
    end

    assert_redirected_to konsuman_path(assigns(:konsuman))
  end

  test "should show konsuman" do
    get :show, id: @konsuman
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @konsuman
    assert_response :success
  end

  test "should update konsuman" do
    patch :update, id: @konsuman, konsuman: { name: @konsuman.name, status: @konsuman.status, uuid: @konsuman.uuid }
    assert_redirected_to konsuman_path(assigns(:konsuman))
  end

  test "should destroy konsuman" do
    assert_difference('Konsuman.count', -1) do
      delete :destroy, id: @konsuman
    end

    assert_redirected_to konsumen_path
  end
end

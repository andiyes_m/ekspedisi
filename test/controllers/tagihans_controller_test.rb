require 'test_helper'

class TagihansControllerTest < ActionController::TestCase
  setup do
    @tagihan = tagihans(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tagihans)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tagihan" do
    assert_difference('Tagihan.count') do
      post :create, tagihan: { customer_name: @tagihan.customer_name, id: @tagihan.id, no_tagihan: @tagihan.no_tagihan, tgl_tagihan: @tagihan.tgl_tagihan }
    end

    assert_redirected_to tagihan_path(assigns(:tagihan))
  end

  test "should show tagihan" do
    get :show, id: @tagihan
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tagihan
    assert_response :success
  end

  test "should update tagihan" do
    patch :update, id: @tagihan, tagihan: { customer_name: @tagihan.customer_name, id: @tagihan.id, no_tagihan: @tagihan.no_tagihan, tgl_tagihan: @tagihan.tgl_tagihan }
    assert_redirected_to tagihan_path(assigns(:tagihan))
  end

  test "should destroy tagihan" do
    assert_difference('Tagihan.count', -1) do
      delete :destroy, id: @tagihan
    end

    assert_redirected_to tagihans_path
  end
end

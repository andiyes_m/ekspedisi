require 'test_helper'

class DaftarMuatansControllerTest < ActionController::TestCase
  setup do
    @daftar_muatan = daftar_muatans(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:daftar_muatans)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create daftar_muatan" do
    assert_difference('DaftarMuatan.count') do
      post :create, daftar_muatan: { customer_name: @daftar_muatan.customer_name, harga: @daftar_muatan.harga, id: @daftar_muatan.id, kendaraan_id: @daftar_muatan.kendaraan_id, kota: @daftar_muatan.kota, no_dm: @daftar_muatan.no_dm, tambahan: @daftar_muatan.tambahan, tgl_kirim: @daftar_muatan.tgl_kirim, tgl_terima: @daftar_muatan.tgl_terima }
    end

    assert_redirected_to daftar_muatan_path(assigns(:daftar_muatan))
  end

  test "should show daftar_muatan" do
    get :show, id: @daftar_muatan
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @daftar_muatan
    assert_response :success
  end

  test "should update daftar_muatan" do
    patch :update, id: @daftar_muatan, daftar_muatan: { customer_name: @daftar_muatan.customer_name, harga: @daftar_muatan.harga, id: @daftar_muatan.id, kendaraan_id: @daftar_muatan.kendaraan_id, kota: @daftar_muatan.kota, no_dm: @daftar_muatan.no_dm, tambahan: @daftar_muatan.tambahan, tgl_kirim: @daftar_muatan.tgl_kirim, tgl_terima: @daftar_muatan.tgl_terima }
    assert_redirected_to daftar_muatan_path(assigns(:daftar_muatan))
  end

  test "should destroy daftar_muatan" do
    assert_difference('DaftarMuatan.count', -1) do
      delete :destroy, id: @daftar_muatan
    end

    assert_redirected_to daftar_muatans_path
  end
end

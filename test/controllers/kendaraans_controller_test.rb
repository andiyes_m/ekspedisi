require 'test_helper'

class KendaraansControllerTest < ActionController::TestCase
  setup do
    @kendaraan = kendaraans(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:kendaraans)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create kendaraan" do
    assert_difference('Kendaraan.count') do
      post :create, kendaraan: { id: @kendaraan.id, no_kendaraan: @kendaraan.no_kendaraan, supir: @kendaraan.supir, tipe: @kendaraan.tipe }
    end

    assert_redirected_to kendaraan_path(assigns(:kendaraan))
  end

  test "should show kendaraan" do
    get :show, id: @kendaraan
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @kendaraan
    assert_response :success
  end

  test "should update kendaraan" do
    patch :update, id: @kendaraan, kendaraan: { id: @kendaraan.id, no_kendaraan: @kendaraan.no_kendaraan, supir: @kendaraan.supir, tipe: @kendaraan.tipe }
    assert_redirected_to kendaraan_path(assigns(:kendaraan))
  end

  test "should destroy kendaraan" do
    assert_difference('Kendaraan.count', -1) do
      delete :destroy, id: @kendaraan
    end

    assert_redirected_to kendaraans_path
  end
end

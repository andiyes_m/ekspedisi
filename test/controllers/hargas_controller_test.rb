require 'test_helper'

class HargasControllerTest < ActionController::TestCase
  setup do
    @harga = hargas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:hargas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create harga" do
    assert_difference('Harga.count') do
      post :create, harga: { id: @harga.id, kota_tujuan: @harga.kota_tujuan, ongkos: @harga.ongkos, tarif: @harga.tarif }
    end

    assert_redirected_to harga_path(assigns(:harga))
  end

  test "should show harga" do
    get :show, id: @harga
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @harga
    assert_response :success
  end

  test "should update harga" do
    patch :update, id: @harga, harga: { id: @harga.id, kota_tujuan: @harga.kota_tujuan, ongkos: @harga.ongkos, tarif: @harga.tarif }
    assert_redirected_to harga_path(assigns(:harga))
  end

  test "should destroy harga" do
    assert_difference('Harga.count', -1) do
      delete :destroy, id: @harga
    end

    assert_redirected_to hargas_path
  end
end
